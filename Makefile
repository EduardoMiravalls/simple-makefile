#
# Author: Eduardo Miravalls Sierra
# Date: 2014-11-07 16:27
#
# Simple Makefile
#
# The MIT License (MIT)
#
# Copyright (c) 2014 Eduardo Miravalls Sierra
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

.PHONY: debug all release clean

# C Compiler
CC     := gcc
LINKER := gcc
# Compiler Flags
CFLAGS := -Wall -Wextra
# Linker flags
LFLAGS :=
# Add libraries here. eg: -lm
LIBS   :=

# Directory were the .o files will be placed
OBJDIR :=obj/

# Add the directories with your header files (if any).
INC    :=
INC    :=$(addprefix -I, $(INC))

# Find, recursively, all '.c' files
SRC     :=$(shell find ./ -type f -name '*.c' )
# Remove leading ./
SRC     :=$(SRC:./%=%)
# Generate filenames for object files
OBJ     :=$(addprefix $(OBJDIR), $(SRC:.c=.o))

# Add the name of you executable name
EXE    :=

all:	$(EXE)

$(EXE):	$(OBJ)
	$(CC) $(LFLAGS) $^ -o $@ $(LIBS)

debug:	CFLAGS += -g
debug:	all

release:	CFLAGS += -O2
release:	all

# Pattern rule to generate object files
$(OBJDIR)%.o:	%.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INC) -c $^ -o $@

clean:
	find $(OBJDIR) -type f -delete
	rm $(EXE)
